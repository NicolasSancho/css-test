import image from "./../product.png";
import { useState, Fragment } from "react";
import Slider from "react-slick";
import "./Carousel.css";

const Carousel = (props) => {
  const [nav1, setNav1] = useState();
  const [nav2, setNav2] = useState();

  const renderThumbnail = () => {
    var images = [];
    for (var i = 0; i < props.images; i++) {
      images.push(
        <img className="carousel-thumbnail" width="80" key={i} src={image} />
      );
    }
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 7,
      focusOnSelect: true,
      slidesToScroll: 1,
      arrows: false,
    };
    return (
      <Slider asNavFor={nav1} ref={(slider2) => setNav2(slider2)} {...settings}>
        {images}
      </Slider>
    );
  };
  const renderCarousel = () => {
    const settings = {
      dots: false,
      dotsClass: "slick-dots slick-thumb",
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    if (props.images < 2) {
      return (
        <div className="carousel-image">
          <img alt="image" src={image} />
        </div>
      );
    }
    return (
      <Fragment>
        <Slider
          asNavFor={nav2}
          ref={(slider1) => setNav1(slider1)}
          {...settings}
        >
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
          <div className="carousel-image">
            <img alt="image" src={image} />
          </div>
        </Slider>
        <div className="thumbnails-carousel-container">{renderThumbnail()}</div>
      </Fragment>
    );
  };

  return renderCarousel();
};
export default Carousel;
