import image from "./../product.png";
import { Fragment } from "react";
import Slider from "react-slick";
import "./OtherProducts.css";
const OtherProducts = (props) => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: props.items < 4 ? props.items : 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 7000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: props.items < 2 ? props.items : 2,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 7000,
          dots: false,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 7000,
          dots: false,
        },
      },
    ],
  };

  const renderItems = () => {
    var items = [];
    for (var i = 0; i < props.items; i++) {
      items.push(
        <div className="item" key={i}>
          <div className="image" key={i + 1}>
            <img src={image} alt="" key={i + 2} />
          </div>
          <div className="card-details" key={i + 3}>
            <span className="card-brand" key={i + 4}>
              Brand
            </span>
            <span className="card-title" key={i + 5}>
              Some title about the specific product
            </span>
            <span className="price-base" key={i + 6}>
              Base price € 110.00
            </span>
            <span className="price-value" key={i + 7}>
              € 15.00
            </span>
            <span className="price-link" key={i + 8}>
              Link
            </span>
          </div>
        </div>
      );
    }
    if (props.items < 1) return null;
    return (
      <Fragment>
        <div className="other-products-container">
          <h4 className="h4-heading-bold">Other products:</h4>
          <div className="other-products-items">
            <Slider {...settings}>{items}</Slider>
          </div>
        </div>
        <hr />
      </Fragment>
    );
  };
  return renderItems();
};
export default OtherProducts;
