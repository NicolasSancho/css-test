import logo from "./logo.png";
import icon from "./vectorpaint.svg";
import Carousel from "./carousel/Carousel.js";
import OtherProducts from "./otherProducts/OtherProducts.js";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./App.css";

function App() {
  //IMPORTANT VARIABLES TO HANDLE BEHAVIORS

  //IMAGES
  // > 2 WILL SHOW IMAGES CAROUSEL
  const images = 9;
  //ITEMS
  // < 1 WILL HIDE PRODUCTS CAROUSEL
  const items = 3;

  return (
    <div className="page-content">
      <div className="header">
        <a href="#">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="14"
            height="14"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            className="icon-icon-2Oo"
          >
            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
            <polyline points="9 22 9 12 15 12 15 22"></polyline>
          </svg>
        </a>
        <a href="#" className="back">
          {" > oneThing > otherThing"}
        </a>
      </div>
      <div className="product-details-full">
        <div className="title">
          <section>
            <h1>Some title here</h1>
          </section>
        </div>
        <div className="product-carousel-container">
          <Carousel images={images} />
        </div>
        <div className="product-price">
          <p>
            <span className="price-base">Base price € 110.00</span>
          </p>
          <span className="price-value">€ 53,90</span>
          <p className="price-disscount">
            Discount € 56,1
            <span className="price-icon-v-7 icon-root-1sI">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="18"
                height="18"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="icon-icon-2Oo"
              >
                <circle cx="12" cy="12" r="10"></circle>
                <line x1="12" y1="8" x2="12" y2="12"></line>
                <line x1="12" y1="16" x2="12.01" y2="16"></line>
              </svg>
            </span>
          </p>
        </div>
        <div className="product-date">
          <section className="date-info">
            <p>
              Date info here
              <span className="price-icon-v-7 icon-root-1sI">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="icon-icon-2Oo"
                >
                  <circle cx="12" cy="12" r="10"></circle>
                  <line x1="12" y1="8" x2="12" y2="12"></line>
                  <line x1="12" y1="16" x2="12.01" y2="16"></line>
                </svg>
              </span>
            </p>
          </section>
        </div>
        <div className="product-brand">
          <div className="brand-text">
            <span>L</span>
            <span>í</span>
            <span>n</span>
            <span>í</span>
            <span>e</span>
          </div>
          <img src={logo} alt="logo" />
        </div>
        <div className="product-button">
          <section className="add-button">
            <button type="button" id="button">
              <span className="button-content">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="icon-icon-2Oo"
                >
                  <circle cx="9" cy="21" r="1"></circle>
                  <circle cx="20" cy="21" r="1"></circle>
                  <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                </svg>
              </span>
              <span className="button-content"> Add to cart </span>
            </button>
          </section>
        </div>
        <div className="product-description">
          <h3>
            <strong>Description:</strong>
          </h3>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt
            odio consequatur provident necessitatibus nemo dolorem quam, ab
            iusto incidunt omnis velit reprehenderit, quasi quos quidem
            laudantium eaque cum delectus, sapiente distinctio quis veritatis
            labore suscipit natus accusamus? Expedita quod possimus, minima
            repellendus voluptates ad, mollitia debitis quam inventore aliquam
            perferendis.
          </p>
        </div>
        <div className="product-pros">
          <h3>
            <strong>Pros:</strong>
          </h3>
          <ul>
            <li>
              <span className="pros-icon">+</span> one thing here
            </li>
            <li>
              <span className="pros-icon">+</span> one thing here
            </li>
            <li>
              <span className="pros-icon">+</span> one thing here
            </li>
          </ul>
        </div>
      </div>
      <hr />
      <OtherProducts items={items} />
      <div className="product-specifications">
        <h4 className="h4-heading-bold">Specifications:</h4>
        <table>
          <tbody>
            <tr>
              <td>Table text example</td>
              <td>Table text example</td>
            </tr>
            <tr>
              <td>They combine with the inbuilt reservoir on two lines</td>
              <td>Table text example</td>
            </tr>
            <tr>
              <td>Table text example</td>
              <td>Table text example</td>
            </tr>
            <tr>
              <td>Table text example</td>
              <td>Table text example</td>
            </tr>
            <tr>
              <td>Table text example</td>
              <td>Table text example</td>
            </tr>
            <tr>
              <td>Table text example</td>
              <td>Table text example</td>
            </tr>
          </tbody>
        </table>
      </div>
      <hr />
      <div className="downloads-container">
        <h4 className="h4-heading-bold">Downloads</h4>
        <h4 className="texto">Some text over here</h4>
        <ul>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
        </ul>
        <h4 className="texto">Some text over here</h4>
        <ul>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
          <li>
            <span>
              <img src={icon} alt="Icon1" />
              Document 1
            </span>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default App;
